for i in vendor/4thewin/*/vendor; do
  url="file://\$MODULE_DIR\$/$i";
  xmlstarlet ed --inplace -s '//content' -t elem -n 'excludeFolder' -i '//content/excludeFolder[last()]' -t attr -n url -v "$url" .idea/*.iml
done