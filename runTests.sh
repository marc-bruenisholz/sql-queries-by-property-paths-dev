#!/bin/bash


RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

repositoryNames=$(cat topologicalOrder.txt)

for repositoryName in $repositoryNames; do
  echo -e "${BLUE}CHECKING${NC} ${repositoryName}.."
  path="repositories/$repositoryName"
  pushd "$path" > /dev/null || { echo "Could not change directory to $path"; exit 1; }

  if gitOutput=$(git status --porcelain) && [ -z "$gitOutput" ]; then
    if ../../update.sh; then
      if [ -d vendor/phpunit/phpunit/ ]; then
        phpUnitOutput=$(XDEBUG_MODE=coverage php vendor/phpunit/phpunit/phpunit --coverage-text --whitelist src/ tests/)
        testResult=$(echo "$phpUnitOutput" | sed -n '7,7p' | cut -d ' ' -f 1)
        if [ "$testResult" = "OK" ]; then
          coverageResult=$(echo "$phpUnitOutput" | sed -n '3,3p' | sed -r 's/.*\(([0-9]+)%\).*/\1/')
          if [ "$coverageResult" = "100" ]; then
            echo -e "${GREEN}OK${NC} $repositoryName"
          else
            echo -e "${RED}MISSING COVERAGE${NC}: $repositoryName"
            echo "$phpUnitOutput"
            exit 1
          fi
        else
          echo -e "${RED}FAILING TESTS${NC}:$repositoryName"
          echo "$phpUnitOutput"
          exit 2
        fi
      else
        echo -e "${GREEN}OK (NO PHPUNIT)${NC} $repositoryName"
      fi
    else
      echo -e "${RED}COMPOSER UPDATE FAILED${NC}:$repositoryName"
    fi
  else
    echo -e "${RED}WORKING DIRECTORY NOT CLEAN${NC}:$repositoryName"
    echo "$gitOutput"
    exit 3
  fi
  popd > /dev/null || { echo "Could not revert directory change"; exit 1; }
done

# The following has been converted to a normal PHPUnit test
#pushd "repositories/sqpp-input-output-testing" > /dev/null || { echo "Could not change directory to $path"; exit 1; }
#echo "Running output prediction test.."
#php src/minimumSchemaEndToEndTest.php
#echo -e "${GREEN}OK${NC}"
#popd > /dev/null || { echo "Could not revert directory change"; exit 1; }


