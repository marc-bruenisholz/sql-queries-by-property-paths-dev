#!/bin/bash

full_path=$(realpath "$0")
dir_path=$(dirname "$full_path")


repositoryNames=$(cat topologicalOrder.txt)

for repositoryName in $repositoryNames; do

    pushd "repositories/$repositoryName" > /dev/null || { echo "Could not change directory to repositories/$repositoryName"; exit 1; }
    git tag "$1"
    popd  > /dev/null || { echo "Could not change directory to $dir_path"; exit 1; }

done


