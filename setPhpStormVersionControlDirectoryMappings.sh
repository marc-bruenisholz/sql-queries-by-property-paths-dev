
xmlstarlet ed --inplace -d '//component[@name="VcsDirectoryMappings"]/mapping' .idea/vcs.xml
for i in repositories/*; do
  directory="\$PROJECT_DIR\$/$i";
  xmlstarlet ed --inplace -s '//component[@name="VcsDirectoryMappings"]' -t elem -n 'mapping' \
  -i '//component[@name="VcsDirectoryMappings"]/mapping[last()]' -t attr -n directory -v "$directory" \
  -i '//component[@name="VcsDirectoryMappings"]/mapping[last()]' -t attr -n vcs -v "Git" .idea/vcs.xml

done