#!/bin/bash

full_path=$(realpath "$0")
dir_path=$(dirname "$full_path")

if [[ ! -f "phpcov.phar" ]]; then
   wget https://phar.phpunit.de/phpcov.phar
fi

if [[ ! -d "coverage-data" ]]; then
  mkdir "coverage-data"
fi

if [[ ! -d "coverage-html" ]]; then
  mkdir "coverage-html"
fi

rm -f coverage-data/*
rm -rf coverage-html/*


repositoryNames=$(cat topologicalOrder.txt)

for repositoryName in $repositoryNames; do
  if [[ -d "repositories/$repositoryName/tests" ]]
  then
    pushd "repositories/$repositoryName" > /dev/null || { echo "Could not change directory to repositories/$repositoryName"; exit 1; }
    vendor/phpunit/phpunit/phpunit --coverage-filter "src" --coverage-php "$dir_path/coverage-data/$repositoryName.cov" "tests"
    popd  > /dev/null || { echo "Could not change directory to $dir_path"; exit 1; }

  fi

done

php phpcov.phar merge --html coverage-html coverage-data
