#!/bin/bash

# git and php composer have to be installed globally

repositoriesDirName="repositories"

mkdir "$repositoriesDirName"
pushd "$repositoriesDirName"  || { echo "Could not change directory to $repositoriesDirName"; exit 1; }

repositoryNames=$(cat ../topologicalOrder.txt)

for repositoryName in $repositoryNames
do
  git clone "git@bitbucket.org:marc-bruenisholz/$repositoryName.git"
  pushd "$repositoryName" > /dev/null || { echo "Could not change directory to $repositoryName"; exit 1; }
  composer install
  popd > /dev/null || { echo "Could not revert directory change"; exit 1; }
done

popd > /dev/null || { echo "Could not revert directory change"; exit 1; }

./runTests.sh



