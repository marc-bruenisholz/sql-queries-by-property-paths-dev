#!/bin/bash

if composerResult=$(composer update --no-ansi 2>&1); then
  if gitResult="$(git status --porcelain)" && [ "$gitResult" = " M composer.lock" ]; then
      if git add composer.lock  > /dev/null; then
        if ! git commit -m "update composer.lock" > /dev/null; then
          echo "composer.lock commit failed"
        fi
      else
        echo "Adding composer.lock failed"
      fi
  fi
else
  echo "composer update failed: $composerResult"
  exit 1
fi