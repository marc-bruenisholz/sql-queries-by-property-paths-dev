# Summary

SQPP ist the abbreviation for "SQL queries by property paths". An expression that was used
for the preceding project. Meanwhile, this software does not use paths anymore, but tree structures that are
constructed in JSON.

Object-relational mappers (ORM) allow developers to specify relationships between entities and
provides an abstraction to read and write objects from/to a relational database without having
the need to craft SQL queries manually.

This project aims to provide software packages to run API servers which allow stating exactly which properties of an
object hierarchy should be read from the database 
(Same as GraphQLs goal "gives clients the power to ask for exactly what they need and nothing more", https://graphql.org/).

An adapter may retrieve the relationship data from the ORM
framework Doctrine but other means to provide ORM data are possible.
The software figures out which tables should be joined and which columns should be selected. 
Furthermore, it converts the result into partial (hierarchical) objects.

This library supports filter conditions that are applied to JOIN conditions or global search conditions that are
applied to the WHERE clause of the SELECT. Aggregations, sorting and pagination are also supported.

Security considerations have been made. There are interfaces to implement row-based or field-based authorization 
methods.

# Integration

SQPP provides a PSR-7 interface to process HTTP requests and return HTTP responses. 
The provided test server uses the package nyholm/psr7-server to convert PHP globals into a
PSR-7 compliant request object but any other HTTP server supporting PSR-7 may be used with SQPP as well.

The interface AccountDataExtractorInterface is used to provide user data.
The class `\ch\_4thewin\SqlQueriesByPropertyPaths\ModAuthOpenidcAccountDataExtractor` may be used to extract
user data from OIDC access tokens.

SQPP defines the interface it uses to connect to databases. See package sqpp-mysql-db-connector or
sqpp-sqlite-db-connector for example implementations of database connectors.

Custom field-based authorization can be implemented using DoctrinePropertyPermissionsInterface (the name
Doctrine will be dropped from the interface as it is not Doctrine specific anymore). An object instance of your
row-based authorization implementation may be passed to your ORM data provider implementation where it checks
user access before ORM data is provided. 

An ORM data provider implementation based on Doctrine can be found in package doctrine-orm-data-provider.
Use ORMDataProviderInterface to implement your own ORM data provider.

Custom row-based authorization can be implemented with AccessControlConditionCreationInterface. 
Return an SQL conditions to limit what data rows a user may access. The SQL
condition can be built with classes from package sqpp-sql-expression-building-blocks.

The query breadth and depth can be limited. Create an object LimitationConfig and pass it to the request handler as well.

An incomplete example is given below:

      // Allow access for users with role admin to any entity and users with role user only
      // to entity "User". All fields of the allowed entity are accessible.
      $roleReceiver = new class implements DoctrinePropertyPermissionsInterface {
        protected array $roles;

        function isAuthorizedToAccessEntity(string $entityName): bool
        {
            if(in_array('admin', $this->roles)) {
                return true;
            } elseif(in_array('user', $this->roles) && $entityName === 'User') {
                return true;
            } else {
                return false;
            }
        }

        function isAuthorizedToAccessProperty(string $entityName, string $propertyName): bool
        {
            return true;
        }

        /**
         * @return array
         */
        public function getRoles(): array
        {
            return $this->roles;
        }

        /**
         * @param array $roles
         */
        public function setRoles(array $roles): void
        {
            $this->roles = $roles;
        }
    };
    // Pass the field-based authorizer to the orm data provider.
    $ormDataProvider = new class($roleReceiver) implements ORMDataProviderInterface {
        protected DoctrinePropertyPermissionsInterface $roleReceiver;
        /**
         * @param DoctrinePropertyPermissionsInterface $roleReceiver
         */
        public function __construct(DoctrinePropertyPermissionsInterface $roleReceiver)
        {
            $this->roleReceiver = $roleReceiver;
        }
        
        function internal_getEntityData(string $entityName): ORMEntity {
            if ($entityName === 'User') {
                return new ORMEntity(
                    'User', // Entity name
                    new ORMTable(
                        'User_Table', // Table name
                        [new ORMColumn('id', 'string', false)] // Primary key columns
                    )
                );
            } 
            throw new ORMInvalidEntityException("Entity not mapped: $entityName");
        }

        function getEntityData(string $entityName): ORMEntity
        {
            // Throws an exception if user has no access. Exception is indistinguishable 
            // from the exception that is thrown if the entity or property does not exist.
            if(!$this->roleReceiver->isAuthorizedToAccessEntity($entityName)) {
                throw new ORMInvalidEntityException("Cannot access entity $entityName");
            }
            return $this->internal_getEntityData($entityName);
        }

        function getPropertyData(string $entityName, string $propertyName): ORMProperty
        {
            if(!$this->roleReceiver->isAuthorizedToAccessProperty($entityName, $propertyName)) {
                throw new ORMInvalidPropertyException("Cannot access $propertyName on entity $entityName");
            }
            if ($entityName === 'User' && $propertyName === 'name') {
                return new ORMField( // Use classes derived from ORMAssociation for associations.
                    $this->internal_getEntityData('User'),
                    'name', // property/field name
                    new ORMColumn(
                        'name', // table column name
                        'string', // column type
                        true // nullable?
                    )
                );
            }
            throw new ORMInvalidPropertyException("Property '$propertyName' is not mapped on entity '$entityName'.");
        }

    };
    // If a user without the role "admin" accesses the table "User" an SQL condition is
    // built to limit results to rows with the user id in the column "id"
    // Currently AccessControlConditionCreationInterface has the functions getRoles and setRoles but
    // they will probably be removed in the future as the roles are now available in accountData.
    $accessControlConditionCreation = new class implements AccessControlConditionCreationInterface {
        protected AccountData $accountData;
        function createAccessControlCondition(Table $table): ?ParameterizedSqlInterface
        {
            if(!in_array('admin', $this->accountData->getRoles()) && $table->getName() === 'User') {
                return new _EQ(
                    new ColumnExpression($table, 'id', 'string'),
                    new \ch\_4thewin\SqlSelectModels\Arguments\StringArgument($this->accountData->getUserId())
                );
            } else {
                return null;
            }
        }

        /**
         * @return array
         */
        public function getRoles(): array
        {
            return [];
        }

        /**
         * @param array $roles
         * @return AccessControlConditionCreationInterface
         */
        public function setRoles(array $roles): AccessControlConditionCreationInterface
        {
            return $this;
        }

        function setAccountData(AccountData $accountData): AccessControlConditionCreationInterface
        {
            $this->accountData = $accountData;
            return $this;
        }
    };
    $limitationConfig = new LimitationConfig(
    // TODO arbitrary limits. What would be reasonable values?
        2,
        5,
        5
    );

    $requestHandler = new RequestHandler($mysqlDBConnector, $ormDataProvider,
        'id', $modAuthOpenidcAccountDataExtractor, $roleReceiver, $accessControlConditionCreation,
        $limitationConfig);



    $response = $requestHandler->handle($serverRequest, true);
    http_response_code($response->getStatusCode());
    echo $response->getBody();
    exit();



# SQLQueriesByPropertyPaths Development Environment

This is the development environment for the project SQLQueriesByPropertyPaths 
which consists of several repositories.

The development environment provides a bootstrap script to download, install and test all
participating repositories.

Before running `./bootstrap.sh`, make *bootstrap.sh* and *runTests.sh* executable.

The script creates a folder *repositories* in the working directory and places the downloaded repositories there.

The `composer.json` file that exists in each repository contains the following config so that the local files are 
used:

      "repositories": [
          {
              "type": "path",
              "url": "../*"
          }
      ],

This way, the dependencies are not copied but symlinked into the *vendor* folder and changes in one local repository are 
reflected immediately in any other dependant repository. A disadvantage of this is that indexing lasts forever unless
symlinked repositories are excluded. The script `setPhpStormVersionControlDirectoryMappings.sh` helps to exclude 
project repositories in the vendor folder from indexing. Exclude the repositories before running `composer install` or `composer update`
because PhpStorm starts indexing immediately. To restart indexing, restart PhpStorm.

`./runTests.sh` can be run separately. It does the following on every repository 
(except the sql-queries-by-property-paths-dev repo):

 - check if working directory is clean
 - composer update
 - run test
 - check coverage
 
# Project Structure

The project is divided into small sub repositories for
better maintenance and usability. The repository *sqpp-test-server* 
contains currently the highest abstraction.
 

The current dependency graph (edges implied by transitivity excluded):
 
![Dependency graph](graph.png)  

| Package name                                      | Description                                                                                                                                                                                |
|---------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| sqpp-test-server                                  | Contains a PHP script to start the HTTP test server. Integrates the Doctrine adapter and  the SQLite connector.                                                                            |
| doctrine-orm-data-provider                        | Implements the interface from the package orm-data-provider-interface to provide ORM data from Doctrine annotations on PHP classes.                                                        |
| sqpp-test-data                                    | Contains the test database with a minimal schema and the corresponding PHP classes that are decorated with Doctrine annotations.                                                           |
| sql-queries-by-property-paths                     | The main package. Uses other packages to provide a PSR-7 interface to process HTTP requests.                                                                                               |
| sqpp-sqlite-db-connector                          | Implements the interface sqpp-db-connector-interface to connect with a SQLite database. It's a thin layer above the PHP standard library SQlite3 to bind arguments to prepared statements. |
| doctrine-property-permissions                     | Extracts permission information from Doctrine annotations on PHP classes to authorize users with certain roles on certain fields.                                                          |
| doctrine-property-permission-annotation           | Defines permission annotations that can be used on PHP classes.                                                                                                                            |
| sqpp-add-orm-data-to-query                        | Extends an SQPP query with ORM data and checks if the SQPP query makes sense together with the ORM data.                                                                                   |
| orm-data-provider-interface                       | Defines the interface for a class that provides ORM data.                                                                                                                                  |
| sqpp-typed-query-to-property-path-tree-conversion | Converts an SQPP query to the internal tree data structure for further processing.                                                                                                         |
| property-path-queries-result-to-object-converter  | Converts database rows into hierarchical objects.                                                                                                                                          |
| property-path-tree-queries-builder                | Creates SQL queries from the internal tree data structure.                                                                                                                                 |
| sqpp-typed-query                                  | Converts an untyped object into a typed SQPP query.                                                                                                                                        |
| sqpp-typed-query-models                           | Defines data models that are used to build typed SQPP queries.                                                                                                                             |
| sqpp-sql-expression-building-blocks               | Contains classes to build SQL condition expressions.                                                                                                                                       |
| property-path-tree-models                         | Defines data models for the internal tree data structure.                                                                                                                                  |
| sql-relationship-query-builder                    | Creats SQL strings from typed objects that contain information about table relationships.                                                                                                  |
| sql-relationship-models                           | Defines data models to represent table relationships for the internal tree data structure.                                                                                                 |
| sql-select-models                                 | Defines data models to represent SELECT SQL queries.                                                                                                                                       |
| sql-select-query-builder                          | Converts SELECT SQL query representations into SQL strings.                                                                                                                                |
| sqpp-db-connector-interface                       | Defines the interface used by SQPP to run SQL queries.                                                                                                                                     |
| tree-traversal                                    | Implements tree traversal (preorder and postorder).                                                                                                                                        |
| sqpp-tree-traversal-models                        | Defines data models and interfaces for tree traversal.                                                                                                                                     |
| sqpp-input-output-testing                         | Generates requests and expected responses systematically and compares them with actual responses.                                                                                          |


# Test server

You can try it. Clone this repository, make `bootstrap.sh` and `runTests.sh` executable, run `./bootstrap.sh`, navigate into 
`repositories/sqpp-test-server` and start a server with 

    php -S localhost:8000 src/testServer.php

Open a terminal in the same directory and send a request with 

    curl -s -H "Expect:" -H "Content-Type: application/json" --request POST -d "@testBody.json"  http://localhost:8000/execute-query | jq

The result should be:

    {
      "baseQuery": "SELECT `T`.`FK1`,`oneToOne`.`SV`,`oneToOne`.`PK`,`oneToOne`.`IV`,`oneToOne`.`BTV`,`oneToOne`.`BFV`,`oneToOne`.`FV`,`T`.`PK`,`T`.`SV`,`T`.`IV`,`T`.`BTV`,`T`.`BFV`,`T`.`FV` FROM `T` JOIN `T` `oneToOne` ON `oneToOne`.`PK` = `T`.`FK1` AND (`oneToOne`.`authorizedRole` IN ('admin','user') AND `oneToOne`.`SV` = 'abc') WHERE (`T`.`authorizedRole` IN ('admin','user') AND (`T`.`SV` > 'a' AND `T`.`IV` >= 123))",
      "aggregateBaseQuery": "",
      "collectionQueries": [],
      "data": {
        "T": [
          {
            "oneToOne": {
              "SV": "abc",
              "PK": 1,
              "IV": 123,
              "BTV": true,
              "BFV": false,
              "FV": 1.3
            },
            "PK": 1,
            "SV": "abc",
            "IV": 123,
            "BTV": true,
            "BFV": false,
            "FV": 1.3
          }
        ]
      }
    }

The test server uses the test database in package sqpp-test-data. The database schema is in the file db-schema.sql.
The ORM mapping is done with Doctrine annotations in the file T.php. The test database has all the supported
associations ManyToOne, OneToMany, ManyToMany and OneToOne.

# SQPP Queries

SQPP Queries are written in JSON. They always start with the key "query". Within "query", multiple entity names 
may be given:

    {
        "query": {
            "User": {},
            "Animal": {}
        }
    }
    
Add properties you want to retrieve:

    {
        "query": {
            "User": {
                "id": {}
            }
        }
    }

If the property is an association, sub properties may be declared:

    {
        "query": {
            "User": {
                "id": {},
                "pictures": {
                    "id": {},
                    "rating": {}
                }
            }
        }
    }

The result might be something like this:

    {
        "data": {
            "User": [
                {
                    "id": 1,
                    "pictures": [
                        {
                            "id": 123,
                            "rating": 6
                        }
                    ]
                },
                {
                    "id": 2,
                    "pictures": [
                        {
                            "id": 124,
                            "rating": 2
                        },
                        {
                            "id": 125,
                            "rating": 1
                        }
                    ]
                }
            ]
        }
    }

The syntax for special features like sort, pagination, filter and aggregation are subject to change:

    {
      "query": {
        "T": {
          "$meta": {
            "aggregate": {
              "IV": [
                "count",
                "sum"
              ]
            },
            "filter": {
              "expression": {
                "SV": {
                  "eq": "abc"
                }
              }
            },
            "sort": [
              ["SV", "ASC"]
            ],
            "page": {
              "limit": 1,
              "offset": 0
            }
          },
          "SV": {}
        }
      }
    }

As you can see, filter expressions may use properties of the current level in the query.
The key in the expression can be a property name of the current level or one of AND, OR, NOT.
Use the special keys to create logical expressions:

    "AND": {
      "SV": {
        "gt": "a"
      },
      "IV": {
        "gte": 123
      }
    }

Comparisons on the top level of the expression are linked with logical OR. Comparisons in NOT are linked
with logical AND. A property may have several comparisons:

    "SV": {
        "gt": "a",
        "lte": "z"
    }

The $meta object under an entity key supports the key "search". It is used
to apply SQL conditions on a result set after all SQL JOINS have been computed.
Use dot notation to access nested properties.

    {
      "query": {
        "T": {
          "$meta": {
            "search": {
              "expression": {
                "oneToMany.SV": {
                  "eq": "abc"
                },
                "oneToMany.BTV": {
                  "eq": false
                }
              }
            }
    ...

A more in-depth description of the syntax will be provided in the future. See file openapidefinition.yaml
in package sql-queries-by-property-paths which contains a OpenAPI definition of the JSON structure.
There are many GraphQL implementations out there. A detailed comparison of how they support
sort, filter, aggregation and pagination will be made and the best of all will be used to implement
the final syntax.

# Known problems / Missing features

- Composite primary keys are not yet supported
- Binary type is converted to string as a workaround
- Missing documentation (I know right ;) )
- Caching is missing
- The important IN comparison operator is not yet implemented
- Special features haven't been tested enough.
- Also test error reporting 
- Cursor-based pagination is not yet supported 
- More security features like token-based limiting, monitoring, profiling
- Optimizations

# Acknowledgement

This project was initially developed at the company clicsoft. They allowed me to make the source code 
public. The code has been changed a lot since then.

At the "Lucerne University for Applied Sciences and Arts", 
I was allowed to continue working on this project.

Thank you very much!

Author: Marc Brünisholz